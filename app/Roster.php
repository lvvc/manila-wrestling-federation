<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roster extends Model
{
    protected $fillable = ['name', 'name_id', 'alias', 'description', 'height', 'weight', 'hometown', 'image', 'champion_image', 'finisher', 'signature_move', 'social_facebook','social_instagram', 'social_twitter', 'is_active', 'faction_id', 'type_id'];

    public function faction()
    {
        return $this->belongsTo('App\Faction');
    }
}
