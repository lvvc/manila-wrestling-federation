<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faction extends Model
{
    protected $fillable = ['name', 'name_id', 'description', 'image', 'finisher', 'signature_move', 'social_facebook','social_instagram', 'social_twitter', 'is_active'];

    public function rosters()
    {
        return $this->hasMany('App\Roster');
    }
}
