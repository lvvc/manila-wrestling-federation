<?php

namespace App\Http\Controllers;

use Auth;
use App\Roster;
use App\Faction;
use Illuminate\Http\Request;

class RosterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roster = Roster::orderBy('name')->get();

        return view('pages.roster.index', compact('roster'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['factions'] = Faction::all();

        return view('pages.roster.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->file('image') != ''){
            $request->validate([
                'name'        => 'required',
                'description' => 'required',
                'height'      => 'required',
                'weight'      => 'required',
                'hometown'    => 'required',
                'image'       => 'image|mimes:jpeg,png,jpg,gif,svg',
            ]);

            $image = time().'.'.$request->image->extension();  
            $request->image->move(public_path('images'), $image);
            $image = 'images/'.$image;
        } else {
            $request->validate([
                'name'        => 'required',
                'description' => 'required',
                'height'      => 'required',
                'weight'      => 'required',
                'hometown'    => 'required',
            ]);

            $image = 'images/default-roster.jpg';
        }

        if ($request->faction_id == 0) {
            $faction_id = NULL;
        } else {
            $faction_id = $request->faction_id;
        }

        $name_id = str_replace(' ', '-', strtolower($request->name));

        $roster_data = array(
            'name'             => $request->name,
            'alias'            => $request->alias,
            'description'      => $request->description,
            'height'           => $request->height,
            'weight'           => $request->weight,
            'hometown'         => $request->hometown,
            'finisher'         => $request->finisher,
            'signature_move'   => $request->signature_move,
            'social_facebook'  => $request->social_facebook,
            'social_instagram' => $request->social_instagram,
            'social_twitter'   => $request->social_twitter,
            'faction_id'       => $faction_id,
            'name_id'          => $name_id,
            'image'            => $image,
        );

        Roster::create($roster_data);

        return redirect()->route('roster.index')->with('success', 'Profile created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Roster  $roster
     * @return \Illuminate\Http\Response
     */
    public function show($name)
    {
        $profile = Roster::where('name_id', $name)->get()->first();

        return view('pages.roster.profile', compact('profile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Roster  $roster
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['factions'] = Faction::all();
        $profile = Roster::findOrFail($id);

        return view('pages.roster.edit', compact('profile', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Roster  $roster
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->file('image') != ''){
            $request->validate([
                'name'        => 'required',
                'description' => 'required',
                'height'      => 'required',
                'weight'      => 'required',
                'hometown'    => 'required',
                'image'       => 'image|mimes:jpeg,png,jpg,gif,svg',
            ]);

            $image = time().'.'.$request->image->extension();  
            $request->image->move(public_path('images'), $image);
            $image = 'images/'.$image;
        } else {
            $request->validate([
                'name'        => 'required',
                'description' => 'required',
                'height'      => 'required',
                'weight'      => 'required',
                'hometown'    => 'required',
            ]);
        }

        if ($request->faction_id == 0) {
            $faction_id = NULL;
        } else {
            $faction_id = $request->faction_id;
        }

        $name_id = str_replace(' ', '-', strtolower($request->name));

        $roster_data = array(
            'name'             => $request->name,
            'alias'            => $request->alias,
            'description'      => $request->description,
            'height'           => $request->height,
            'weight'           => $request->weight,
            'hometown'         => $request->hometown,
            'finisher'         => $request->finisher,
            'signature_move'   => $request->signature_move,
            'social_facebook'  => $request->social_facebook,
            'social_instagram' => $request->social_instagram,
            'social_twitter'   => $request->social_twitter,
            'faction_id'       => $faction_id,
            'name_id'          => $name_id,
        );

        if($request->file('image') != '') { $roster_data['image'] = $image; }

        Roster::whereId($id)->update($roster_data);

        return redirect()->route('roster.show', $name_id)->with('success', 'Profile updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Roster  $roster
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $profile = Roster::findOrFail($id);
        $profile->delete();

        return redirect()->route('roster.index')->with('success', 'Profile deleted successfully!');
    }
}
