<?php

namespace App\Http\Controllers;

use Auth;
use App\Roster;
use App\Faction;
use Illuminate\Http\Request;

class FactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $faction = Faction::orderBy('name')->get();

        return view('pages.faction.index', compact('faction'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.faction.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->file('image') != ''){
            $request->validate([
                'name'        => 'required',
                'description' => 'required',
                'image'       => 'image|mimes:jpeg,png,jpg,gif,svg',
            ]);

            $image = time().'.'.$request->image->extension();  
            $request->image->move(public_path('images'), $image);
            $image = 'images/'.$image;
        } else {
            $request->validate([
                'name'        => 'required',
                'description' => 'required',
            ]);

            $image = 'images/default-faction.jpg';
        }

        $name_id = str_replace(' ', '-', strtolower($request->name));

        $faction_data = array(
            'name'             => $request->name,
            'description'      => $request->description,
            'finisher'         => $request->finisher,
            'signature_move'   => $request->signature_move,
            'social_facebook'  => $request->social_facebook,
            'social_instagram' => $request->social_instagram,
            'social_twitter'   => $request->social_twitter,
            'name_id'          => $name_id,
            'image'            => $image,
        );

        Faction::create($faction_data);

        return redirect()->route('faction.index')->with('success', 'Faction created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Faction  $faction
     * @return \Illuminate\Http\Response
     */
    public function show($name)
    {
        $profile = Faction::where('name_id', $name)->get()->first();

        return view('pages.faction.profile', compact('profile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Faction  $faction
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile = Faction::findOrFail($id);

        return view('pages.faction.edit', compact('profile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Faction  $faction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->file('image') != ''){
            $request->validate([
                'name'        => 'required',
                'description' => 'required',
                'image'       => 'image|mimes:jpeg,png,jpg,gif,svg',
            ]);

            $image = time().'.'.$request->image->extension();  
            $request->image->move(public_path('images'), $image);
            $image = 'images/'.$image;
        } else {
            $request->validate([
                'name'        => 'required',
                'description' => 'required',
            ]);
        }

        $name_id = str_replace(' ', '-', strtolower($request->name));

        $faction_data = array(
            'name'             => $request->name,
            'description'      => $request->description,
            'finisher'         => $request->finisher,
            'signature_move'   => $request->signature_move,
            'social_facebook'  => $request->social_facebook,
            'social_instagram' => $request->social_instagram,
            'social_twitter'   => $request->social_twitter,
            'name_id'          => $name_id,
        );

        if($request->file('image') != '') { $faction_data['image'] = $image; }

        Faction::whereId($id)->update($faction_data);

        return redirect()->route('faction.show', $name_id)->with('success', 'Faction updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Faction  $faction
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $profile = Faction::findOrFail($id);
        $profile->delete();

        return redirect()->route('faction.index')->with('success', 'Faction deleted successfully!');
    }
}
