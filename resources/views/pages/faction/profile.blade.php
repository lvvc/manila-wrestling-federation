@extends('layouts.master')

@section('pageTitle', 'Faction - ' . $profile->name)

@section('content')
    <div class="col-12">
        <h4 class="card-title text-center d-md-none">
            {{ $profile->name }}
        </h4>
        <div class="card card-mwf-img card-mwf-faction">
            <img class="card-img" src="{{ asset($profile->image) }}" alt="{{ $profile->name }}">
            <div class="card-img-overlay text-center d-sm-none d-md-block">
                <h1 class="card-title position-relative">
                    {{ $profile->name }}
                </h1>
            </div>
        </div>
    </div>
    <div class="col-12 font-montserrat">
        <div class="card card-mwf-text">
            <div class="card-body">
                <h1 class="card-title">
                    {{ $profile->name }}
                </h1>
                <p class="card-description">
                    <em>{{ $profile->description }}</em>
                </p>

                @if( $profile->finisher != NULL )
                    <p class="card-text card-mwf-text-label">
                        <strong>Finisher:</strong>
                    </p>
                    <p class="card-text card-mwf-text-value">
                        {{ $profile->finisher }}
                    </p>
                @endif

                @if( $profile->signature_move != NULL )
                    <p class="card-text card-mwf-text-label">
                        <strong>Signature Move:</strong>
                    </p>
                    <p class="card-text card-mwf-text-value">
                        {{ $profile->signature_move }}
                    </p>
                @endif
                
                <div class="row">
                    @foreach ($profile->rosters as $roster)
                        <div class="col-12 col-md-3 my-2">
                            <a class="card card-mwf-img card-mwf-roster" href="{{ route('roster.show', $roster->name_id) }}">
                                <img class="card-img" src="{{ asset($roster->image) }}" alt="{{ $roster->name }}">
                                <div class="card-img-overlay">
                                    <h2 class="card-title">
                                        {{ $roster->name }}
                                    </h2>
                                </div>
                            </a>
                            <h4 class="d-md-none mt-2">
                                {{ $roster->name }}
                            </h4>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
