@extends('layouts.master')

@section('pageTitle', 'Edit Faction')

@section('content')
    <form class="form-row" action="{{ route('faction.update', $profile->id) }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('PATCH')
        <div class="col-12">
            <div class="card card-mwf-img card-mwf-faction">
                <img class="card-img" src="{{ asset($profile->image) }}">
            </div>
            <div class="custom-file my-3">
                <label for="image" class="custom-file-label">
                    <h4>
                        {{ __('Upload Image') }}
                    </h4>
                </label>
                <input type="file" name="image" class="custom-file-input" id="image" accept="image/*">
                @error('image')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="col-12 font-montserrat">
            <div class="card card-mwf-text">
                <div class="card-body">
                    <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">
                            <h1>
                                {{ __('Name:') }}
                            </h1>
                        </label>
                        <div class="col-sm-10">
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $profile->name }}" required autocomplete="name" autofocus>
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="description" class="col-sm-2 col-form-label">
                            <p>
                                {{ __('Description:') }}
                            </p>
                        </label>
                        <div class="col-sm-10">
                            <textarea id="description" type="text" class="form-control @error('description') is-invalid @enderror" name="description" value="{{ $profile->description }}" required autocomplete="description">{{ $profile->description }}</textarea>
                            @error('description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <div class="col-12">
                            <label for="finisher" class="col-sm-2 col-form-label">
                                <strong>{{ __('Finisher:') }}</stromg>
                            </label>
                            <div class="col-sm-10">
                                <input id="finisher" type="text" class="form-control @error('finisher') is-invalid @enderror" name="finisher" value="{{ $profile->finisher }}" autocomplete="finisher">
                                @error('finisher')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">
                            <label for="signature_move" class="col-sm-2 col-form-label">
                                <strong>{{ __('Signature Move:') }}</stromg>
                            </label>
                            <div class="col-sm-10">
                                <input id="signature_move" type="text" class="form-control @error('signature_move') is-invalid @enderror" name="signature_move" value="{{ $profile->signature_move }}" autocomplete="signature_move">
                                @error('signature_move')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <button class="btn btn-lg btn-block btn-outline-success" type="submit">
                Update Faction
            </button>
        </div>
    </form>
@endsection