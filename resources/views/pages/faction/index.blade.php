@extends('layouts.master')

@section('pageTitle', 'Factions')

@section('content')
    @foreach($faction as $profile)
        <div class="col-12 col-md-6 my-2">
            <a class="card card-mwf-img card-mwf-faction" href="{{ route('faction.show', $profile->name_id) }}">
                <img class="card-img" src="{{ asset($profile->image) }}" alt="{{ $profile->name }}">
                <div class="card-img-overlay text-center">
                    <h2 class="card-title position-relative">
                        {{ $profile->name }}
                    </h2>
                </div>
            </a>
        </div>
    @endforeach
@endsection
