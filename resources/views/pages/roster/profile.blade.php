@extends('layouts.master')

@section('pageTitle', 'Profile - ' . $profile->name)

@section('content')
    <div class="col-12 col-md-3">
        <div class="card card-mwf-img">
            <img class="card-img" src="{{ asset($profile->image) }}" alt="{{ $profile->name }}">
            <div class="card-img-overlay">
                <h2 class="card-title">
                    {{ $profile->name }}
                </h2>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-9 font-montserrat">
        <div class="card card-mwf-text">
            <div class="card-body">
                <h1 class="card-title d-sm-none d-md-block">
                    {{ $profile->name }}
                </h1>
                @if( $profile->alias != NULL )
                <h4 class="card-subtitle text-muted">
                    {{ $profile->alias }}
                </h4>
                @endif
                <p class="card-description">
                    <em>{{ $profile->description }}</em>
                </p>

                <div class="border-top my-3"></div>

                <div class="row">
                    <div class="col-12 col-md-4">
                        <p class="card-text card-mwf-text-label">
                            <strong>Hometown:</strong>
                        </p>
                        <p class="card-text card-mwf-text-value">
                            {{ $profile->hometown }}
                        </p>
                    </div>

                    <div class="col-6 col-md-4">
                        <p class="card-text card-mwf-text-label">
                            <strong>Height:</strong>
                        </p>
                        <p class="card-text card-mwf-text-value">
                            {{ $profile->height }}
                        </p>
                    </div>

                    <div class="col-6 col-md-4">
                        <p class="card-text card-mwf-text-label">
                            <strong>Weight:</strong>
                        </p>
                        <p class="card-text card-mwf-text-value">
                            {{ $profile->weight }}
                        </p>
                    </div>
                </div>

                @if( $profile->finisher != NULL )
                    <p class="card-text card-mwf-text-label">
                        <strong>Finisher:</strong>
                    </p>
                    <p class="card-text card-mwf-text-value">
                        {{ $profile->finisher }}
                    </p>
                @endif

                @if( $profile->signature_move != NULL )
                    <p class="card-text card-mwf-text-label">
                        <strong>Signature Move:</strong>
                    </p>
                    <p class="card-text card-mwf-text-value">
                        {{ $profile->signature_move }}
                    </p>
                @endif
                
                @if( $profile->faction != NULL )
                    <p class="card-text card-mwf-text-label">
                        <strong>Faction:</strong>
                    </p>
                    <p class="card-text card-mwf-text-value">
                        <a href="{{ route('faction.show', $profile->faction->name_id) }}">
                            {{ $profile->faction->name }}
                        </a>
                    </p>
                @endif
            </div>
        </div>
    </div>
@endsection
