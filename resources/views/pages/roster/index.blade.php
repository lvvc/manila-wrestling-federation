@extends('layouts.master')

@section('pageTitle', 'Roster')

@section('content')
    @foreach($roster as $profile)
        <div class="col-6 col-md-3 my-2">
            <a class="card card-mwf-img card-mwf-roster" href="{{ route('roster.show', $profile->name_id) }}">
                <img class="card-img" src="{{ asset($profile->image) }}" alt="{{ $profile->name }}">
                <div class="card-img-overlay">
                    <h2 class="card-title">
                        {{ $profile->name }}
                    </h2>
                </div>
            </a>
        </div>
    @endforeach
@endsection
