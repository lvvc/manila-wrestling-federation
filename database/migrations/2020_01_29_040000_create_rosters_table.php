<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRostersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rosters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->string('name_id')->unique();
            $table->string('alias')->unique()->nullable();
            $table->longText('description');
            $table->string('height');
            $table->string('weight');
            $table->string('hometown');
            $table->string('image')->default('images/default-roster.jpg');
            $table->string('champion_image')->nullable();
            $table->string('finisher')->nullable();
            $table->string('signature_move')->nullable();
            $table->string('social_facebook')->nullable();
            $table->string('social_instagram')->nullable();
            $table->string('social_twitter')->nullable();
            $table->boolean('is_active')->default('1');
            $table->unsignedBigInteger('faction_id')->nullable();
            $table->foreign('faction_id')
                    ->references('id')
                    ->on('factions');
            $table->unsignedBigInteger('type_id')->default('1');
            $table->foreign('type_id')
                    ->references('id')
                    ->on('types');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rosters');
    }
}
