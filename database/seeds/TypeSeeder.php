<?php

use Illuminate\Database\Seeder;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->insert([
            'name'        => 'Wrestler',
            'name_id'     => 'wrestler',
            'description' => 'MWF Wrestler',
            'created_at'  => Carbon\Carbon::now(),
            'updated_at'  => Carbon\Carbon::now(),
        ]);
    }
}
