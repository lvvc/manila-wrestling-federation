<?php

use Illuminate\Database\Seeder;

class FactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('factions')->insert([
        [
            'name_id'          => 'hssl',
            'name'             => 'Hukbong Sandatahan ng mga Salot ng Lipunan (HSSL)',
            'description'      => 'Kami ang Hukbong Sandatahan ng mga Salot ng Lipunan.',
            'image'            => 'images/default-faction.jpg',
            'finisher'         => NULL,
            'signature_move'   => NULL,
            'social_facebook'  => 'https://www.facebook.com/MWFhssl/',
            'social_twitter'   => NULL,
            'social_instagram' => NULL,
            'is_active'        => 1,
            'created_at'       => Carbon\Carbon::now(),
            'updated_at'       => Carbon\Carbon::now(),
        ],
        [
            'name_id'          => 'ang-bahay-ng-liwanag',
            'name'             => 'Ang Bahay ng Liwanag',
            'description'      => 'Ang kaligtasan para sa sanlibutan ay hatid ng Bahay ng Liwanag.',
            'image'            => 'images/default-faction.jpg',
            'finisher'         => NULL,
            'signature_move'   => NULL,
            'social_facebook'  => 'https://www.facebook.com/bahayngliwanag/',
            'social_twitter'   => NULL,
            'social_instagram' => NULL,
            'is_active'        => 1,
            'created_at'       => Carbon\Carbon::now(),
            'updated_at'       => Carbon\Carbon::now(),
        ],
        [
            'name_id'          => 'youngblxxd',
            'name'             => 'YoungBlxxd',
            'description'      => 'Artistic Expressionism & Fighting Chaos with Pleasure',
            'image'            => 'images/default-faction.jpg',
            'finisher'         => NULL,
            'signature_move'   => NULL,
            'social_facebook'  => 'https://www.facebook.com/YoungBlxxd-MNL-107698117237669/',
            'social_twitter'   => NULL,
            'social_instagram' => NULL,
            'is_active'        => 0,
            'created_at'       => Carbon\Carbon::now(),
            'updated_at'       => Carbon\Carbon::now(),
        ],
        ]);
    }
}
