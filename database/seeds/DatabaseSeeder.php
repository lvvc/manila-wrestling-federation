<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TypeSeeder::class);
        $this->call(ChampionshipSeeder::class);
        $this->call(FactionSeeder::class);
        $this->call(RosterSeeder::class);
    }
}
